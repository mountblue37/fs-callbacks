const fs = require('fs')
const path = require('path')

function problem2() {
    fs.readFile(path.join(__dirname, 'lipsum.txt'), 'utf-8', (err, data) => {
        // Reads the lipsum.txt file
        if (err === null) {
            fs.writeFile(path.join(__dirname, 'toUpperCase.txt'), data.toUpperCase(), (err) => {
                // Converts text to uppercase and writes file toUpperCase.txt
                if (err === null) {
                    fs.appendFile(path.join(__dirname, `filenames.txt`), 'toUpperCase.txt\n', (err) => {
                        // Writes the toUpperCase.txt to filenames.txt
                        if (err === null) {
                            console.log(`filename toUpperCase.txt appended successfully`)
                        } else {
                            console.log(err)
                        }
                    })

                    fs.readFile(path.join(__dirname, 'toUpperCase.txt'), 'utf-8', (err, data) => {
                        // Reads the file toUpperCase.txt
                        if (err === null) {
                            fs.writeFile(path.join(__dirname, `lowerCaseFile.json`), JSON.stringify(data.toLowerCase().split('.')), (err) => {
                                // Splits the data from toUpperCase.txt and stores it as an array into lowerCaseFile.json
                                if (err === null) {
                                    fs.appendFile(path.join(__dirname, `filenames.txt`), `lowerCaseFile.json\n`, (err) => {
                                        // Writes the lowerCaseFile.json to filenames.txt
                                        if (err === null) {
                                            console.log(`filename lowerCaseFile.json appended successfully`)
                                        } else {
                                            console.log(err)
                                        }
                                    })

                                    fs.readFile(path.join(__dirname, `lowerCaseFile.json`), 'utf-8', (err, data) => {
                                        // Reads the lowerCaseFile.json
                                        if (err === null) {
                                            const toBeSorted = JSON.parse(data)
                                            toBeSorted.sort((sent1, sent2) => {
                                                if (sent1 < sent2) {
                                                    return -1
                                                }
                                                if (sent2 < sent1) {
                                                    return 1
                                                }
                                                return 0
                                            })

                                            fs.writeFile(path.join(__dirname, `lowerCaseFileSorted.json`), JSON.stringify(toBeSorted), (err) => {
                                                // Writes the sported data from lowerCaseFile.json and writes it to lowerCaseFileSorted.json file
                                                if (err === null) {
                                                    fs.appendFile(path.join(__dirname, `filenames.txt`), `lowerCaseFileSorted.json`, (err) => {
                                                        // Writes lowerCaseFileSorted.json to filenames.txt
                                                        if (err === null) {
                                                            console.log(`filename lowerCaseFileSorted.json appended successfully`)
                                                            fs.readFile(path.join(__dirname, 'filenames.txt'), 'utf-8', (err, data) => {
                                                                // Reads the filename.txt and deletes all files after 3 seconds
                                                                if (err === null) {
                                                                    data.split('\n').forEach((filename) => {
                                                                        fs.unlink(path.join(__dirname, filename), (err) => {
                                                                            if (err) {
                                                                                console.error(err)
                                                                            }
                                                                        })
                                                                    })
                                                                    fs.unlink(path.join(__dirname, 'filenames.txt'), (err) => {
                                                                        if (err === null) {
                                                                            console.log('filenames.txt removed successfully')
                                                                        } else {
                                                                            console.log(err)
                                                                        }
                                                                    })
                                                                } else {
                                                                    console.error(err)
                                                                }
                                                            })
                                                        } else {
                                                            console.log(err)
                                                        }
                                                    })

                                                } else {
                                                    console.error(err)
                                                }
                                            })
                                        } else {
                                            console.error(err)
                                        }
                                    })

                                } else {
                                    console.error(err)
                                }
                            })
                        } else {
                            console.error(err)
                        }
                    })
                } else {
                    console.error(err)
                }
            })
        } else {
            console.error(err)
        }
    })
}

module.exports = problem2