const fs = require('fs')
const path = require('path')

function deleteFiles(fileNames) {
    fileNames.forEach((fileName, index) => {
        fs.unlink(path.resolve(__dirname, 'randomFiles', `${fileName}.json`), (err) => {
            if (err === null && index === fileNames.length - 1) {
                console.log(`All files deleted successfully`)
            } else if (err) {
                console.error(err)
            }
        })
    })
}

function makeAndDelete(fileNames) {
    fs.mkdir(path.join(__dirname, 'randomFiles'), (err) => {
        if (err === null || err.code === 'EEXIST') {
            fileNames.forEach((fileName, index) => {
                fs.writeFile(path.resolve(__dirname, 'randomFiles', `${fileName}.json`), JSON.stringify({ message: `file ${fileName}.json has been created` }), (err) => {
                    if (err === null && index === fileNames.length - 1) {
                        console.log(`All files created successfully successfully`)
                        deleteFiles(fileNames)
                    } else if (err) {
                        console.error(err)
                    }
                })
            })

        } else {
            console.error(err)
        }
    })
}

function problem1() {
    let random = Math.random()
    const fileNames = []
    for (let i = 1; i < random * 100; i++) {
        fileNames.push(i)
    }
    makeAndDelete(fileNames)
}

module.exports = problem1